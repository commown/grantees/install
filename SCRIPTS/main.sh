#! /bin/bash

main() {
    base_install=$1
    popOS_install=$2
    uh="/usr/share"

    if [ $popOS_install -eq 1 ];then
        # on fait les choses bien
        TITREGAMING "${PINK}" "${dir}"
    fi

    # Install clevo-specific software on clevo machines only.
    # For now, this test with lspci is preferred over one with dmidecode although
    # the dmi table setup is now part of our protocol, because its more robust
    if lspci -vvv | grep -iw clevo | head -1 > /dev/null ; then
        # COMMOWN + TUXEDO APT REPO (FOR TUXEDO CONTROL CENTER AND MORE)
        apt_commown_and_tuxedo_install
    fi

    # backgrounds, preferences etc. (also deals with PopOS specifics)
    settings_and_schemas

    apps_install

    ## disabled until approved -> see 'util-support' branch
    # install_support_shortcuts


    ### OEM "ship to end user" desktop shortcut -> ./oem_shortcut.sh
    if ! check_oem_shortcut;then
        # PopOS doesn't currently have an OEM install mode, so we'll create it
        answerOEM=""
        DS "\nRaccourci bureau pour activer le mode OEM ? (O/n) " && printf " > " && read answerOEM
        if [ "$answerOEM" != "${answerOEM#[Oo]}" ]; then # "#[]", any O or o in 1st position will be dropped.
            install_oem_shortcut
        fi
    else
        DSBU "Raccourci OEM OK"
    fi

}


update() {
    DSBU "\n UPDATE IN PROGRESS. This may take some time..."
    progressSpin & # & for background process
    pS_pid=$!
    echo -e ${blue}'###                       [20%]\r'${rcolor}
    checkCL "apt-get update"
    checkCL "apt-get upgrade -y"
    # if 'apt-get update' failed with exit code 100 check your internet connection
    # apt-get full-upgrade -y (not required)
    echo -e ${blue}'##############            [60%]\r'${rcolor}
    # Removes old kernels (keep one old and current)
    checkCL "apt-get autoremove -y" # check with : uname -r
    echo -e ${blue}'##########################[100%]\r'${rcolor}
    kill ${pS_pid}
    DSBU "UPDATE COMPLETED."
}


apps_install() {
    ### REGULAR APPS

    # Activate Canonical ppa + repositories update
    DS "Activate Canonical ppa + repositories update"
    checkS "sed -ie \"/# deb http:\/\/archive.canonical.com\/ubuntu\ /s/# //\" /etc/apt/sources.list"

    # Install favorite apps from apps_to_install.list and initial setup of libdvd-pkg
    DS "Install favorite apps from apps_to_install.list && initial setup of libdvd-pkg"
    DSBU "\n INSTALLATION IN PROGRESS. This may take some time..."
    progressSpin & # & for background process, aesthetic only
    pS_pid=$!
    FAVS="$dir/RESOURCES/apps_to_install.list"
    DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends $(cat $FAVS) >/dev/null && checkL
    DEBIAN_FRONTEND=noninteractive apt-get -y remove grub2-themes-ubuntustudio >/dev/null && checkL #removing ugly ubuntustudio grub theme that keeps installing itself
    #checkCL "snap install codium --classic" # if one day you need vs-code alternative

    ### PopOS-GAMING APPS

    if [ $popOS_install -eq 1 ];then
        # adding support for 32bits apps, mandatory for steam install
        sudo dpkg --add-architecture i386
        gFAVS="$dir/RESOURCES/apps_to_install_gaming.list"
        DEBIAN_FRONTEND=noninteractive apt-get update >/dev/null && checkL # need to update here because of the added architecture i386
        DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends $(cat $gFAVS) >/dev/null && checkL
    fi

    kill ${pS_pid} # kills 'progressSpin' (aesthetic only)

    update

    DSBU "INSTALLATION COMPLETE."
    # apt-get --fix-broken install && apt-get update && apt-get autoremove -y # better to use ftrace than fix-broken
}

settings_and_schemas() {
    # Installing Microsoft fonts with .deb (sometimes required, avoid errors) && Editing dock apps
    DS "Installing Microsoft fonts && Editing dock apps"
    checkCL "echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections"
    override="99_launcher.favorites.gschema.override"
    checkCL "cp -r $dir/RESOURCES/${override} ${uh}/glib-2.0/schemas/${override}" "glib-compile-schemas $uh/glib-2.0/schemas/"

    # Changing background
    DS "Changing background"
    Bg="${uh}/backgrounds"
    Go="${uh}/glib-2.0/schemas/10_ubuntu-settings.gschema.override"
    checkCL "cp $dir/IMAGES/backgroundCommown* $Bg" "cp -r $dir/RESOURCES/10_ubuntu-settings.gschema.override $Go"
    # in case of Pop_OS install :
    Go2="${uh}/glib-2.0/schemas/50_pop-desktop.gschema.override"
    Go3="${uh}/glib-2.0/schemas/50_pop-session.gschema.override"
    if [ -f $Go2 ];then
        checkCL "cp -r $dir/RESOURCES/50_pop-desktop.gschema.override $Go2"
    fi
    if [ -f $Go3 ];then
        checkCL "cp -r $dir/RESOURCES/50_pop-session.gschema.override $Go3"
    fi
    checkDF "-f" "$Bg/backgroundCommown.png" && checkDF "-f" "$Go" && checkCL "glib-compile-schemas ${uh}/glib-2.0/schemas/"


    if ! check_oem_shortcut;then
        # PopOS doesn't currently have an OEM install mode, so we'll create it
        answerOEM=""
        DS "\nRaccourci bureau pour activer le mode OEM ? (O/n) " && printf " > " && read answerOEM
        if [ "$answerOEM" != "${answerOEM#[Oo]}" ]; then # "#[]", any O or o in 1st position will be dropped.
            install_oem_shortcut
        fi
    else
        DSBU "Raccourci OEM OK"
    fi

}


apt_commown_and_tuxedo_install() {
    DSBU "\n Installing Commown and Tuxedo APT repositories & tuxedo-control-center"
    DEBIAN_FRONTEND=noninteractive apt install -y wget coreutils apt-transport-https

    wget -P /etc/apt/keyrings/ https://deb.commown.coop/deb.commown.coop.asc
    echo 'deb [signed-by=/etc/apt/keyrings/deb.commown.coop.asc arch=amd64] https://deb.commown.coop/ jammy main' > /etc/apt/sources.list.d/commown.list

    cp $dir/RESOURCES/tuxedo-archive-keyring.gpg /etc/apt/trusted.gpg.d/
    echo 'deb https://deb.tuxedocomputers.com/ubuntu jammy main' > /etc/apt/sources.list.d/tuxedo-computers.list
    cat>/etc/apt/preferences.d/99commown<<EOF
Package: tuxedo-keyboard
Pin: origin deb.tuxedocomputers.com
Pin-Priority: 1

Package: tuxedo-drivers
Pin: origin deb.tuxedocomputers.com
Pin-Priority: 1

Package: tuxedo-control-center
Pin: origin deb.tuxedocomputers.com
Pin-Priority: 1

EOF

    DEBIAN_FRONTEND=noninteractive apt update -y && checkCL
    DEBIAN_FRONTEND=noninteractive apt install -y tuxedo-control-center tuxedo-archive-keyring >/dev/null && checkL
}
