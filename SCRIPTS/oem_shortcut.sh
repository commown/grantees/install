#! /bin/bash

check_oem_shortcut () { 

    #returns FALSE if OEM shortcut needs to be installed
    
    if [[ -d /home/oem/Bureau ]];then
    	desbur="/home/oem/Bureau"
    elif [[ -d /home/oem/Desktop ]];then
    	desbur="/home/oem/Desktop"
    else
    	desbur="ErreurDossier"
    fi

    if [ -d /home/oem/ ];then
        if [[ $(ls $desbur) == *oem*.desktop ]];then
	    echo -e ${tCyan}"OEM shortcut installation canceled : shortcut file already exists"${RESETCOLOR}
	    true
        else
            echo -e ${tCyan}"OEM shortcut installation : c'est parti "${RESETCOLOR}
            false
        fi
    else 
        echo -e ${tCyan}"OEM shortcut installation canceled : user is not 'oem'"${RESETCOLOR}
        true
    fi
    
}

install_oem_shortcut () {

    if [[ -d /home/oem/Bureau ]];then
    	desbur="/home/oem/Bureau"
    elif [[ -d /home/oem/Desktop ]];then
    	desbur="/home/oem/Desktop"
    else
    	desbur="ErreurDossier"
    fi

    
    if [ -d "$desbur" ];then
        cp -fv install/RESOURCES/oem-config-prepare-gtk.desktop "$desbur" || printf "\ncopie du raccourci vers le bureau impossible\n"

        if [ -f "$desbur"/oem-config-prepare-gtk.desktop ];then
        
            # steps necessary to make the desktop shortcut launchable (equivalent to right-click and "allow launching")
            # see : https://askubuntu.com/questions/1218954/desktop-files-allow-launching-set-this-via-cli

            # only the connected user (and not root) can do that somehow
            # see : https://askubuntu.com/questions/1202488/how-to-mark-a-desktop-file-as-trusted-from-command-line-on-ubuntu-18-04
            sudo -u oem -g oem dbus-launch gio set "$desbur"/oem-config-prepare-gtk.desktop metadata::trusted true && checkL

            chown oem "$desbur"/oem-config-prepare-gtk.desktop
            chgrp oem "$desbur"/oem-config-prepare-gtk.desktop
            chmod 770 "$desbur"/oem-config-prepare-gtk.desktop # Pop or Ubuntu won't allow launching if "others" have rights on the file
        fi
    else
        printf "\n/home/oem/Bureau/ (ou /home/oem/Desktop/) n'existe pas ! il va falloir finir manuellement\n"
        printf "\n\n>>> Création du raccourci d'installation OEM\n  > Instructions manuelles :\n\n"
        printf " 1. Si le raccourci n'apparaît pas sur le bureau : copier le fichier ginstall/RESOURCES/oem-config-prepare.desktop vers le Bureau de l'utilisateur\n"
        printf " 2. Si le raccourci apparaît avec une croix rouge / n'est pas exécutable d'un simple double-clic : clic droit > 'autoriser l'éxecution'/'allow launching'\n"
        printf "    > Alternative à l'utilisation du raccourci : sudo oem-config-prepare\n\n"
        DSP " [[ Attention ! ]]"
        printf "   L'installation OEM n'efface l'utilisateur pré-existant que si il porte le nom 'oem' !\n"
        printf "   Autrement, le nouvel utilisateur du client cohabitera avec l'ancien\n"
        echo
        DSP "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        echo 
        echo -e ${tCyan}"Appuyer sur entrée pour continuer"${RESETCOLOR}
        echo && read jjjjjjjjjj
        fi
        DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends oem-config oem-config-gtk >/dev/null && checkL

    
    shortcut_check="$(sudo -u oem -g oem dbus-launch gio info "$desbur"/oem-config-prepare-gtk.desktop | grep trusted)" || shortcut_check="false"
    warning=0
    
    if [[ $shortcut_check != *"true"* ]];then
    	warning=1
    fi
    
    if [ $warning -eq 1 ];then
    	echo
        DSP "ATTENTION"
        DSP "erreur pendant la création du raccourci bureau OEM (Prepare for shipping to end user)"
        DSP "veuillez effectuer la manipulation suivante :"
        DSP "clic-droit > 'Autoriser l'exécution'"
        echo
        DSP "             ^^^^^^^^^^^^^^^^^^^^^^^"
        echo 
        echo -e ${tCyan}"Appuyer sur entrée pour continuer"${RESETCOLOR}
        echo && read jjjjjjjjjj
    fi
}
