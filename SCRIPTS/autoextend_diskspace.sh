#! /bin/bash

##### NE PAS MODIFIER DIRECTEMENT --> améliorer le projet 'autoextend' puis écraser ce fichier

extend_unalloc() {

	test_lvm=''
	test_lvm+="$(lsblk | grep 'lvm')"

	TITREAUTOEXTENSION "${PINK}" "${dir}"
	
	if [[ -n $test_lvm ]];then
	#in case parts are of type LVM (and maybe cyphered with LUKS)
	#the LVM-LUKS procedure is longer, after this 'if' for the classical parts case

		extend_lvm

	else
	
		extend_regular
		
	fi	

#extend_unalloc end

}

extend_lvm() {
	##########################
	#	
	#	A VOIR ICI si on veut remplacer l'acquisition de la valeur $lv par la méthode plus complète développée en bas pour les partitions classiques
	#	-> conséquence de cette question : est-ce que le LV et le mountpoint '/' apparaissent sur la même ligne avec lsblk ? si oui tout devrait bien se passer
	# 	--> COMPLETER D'ABORD le script d'en dessous avec la boucle while
	#	
	##########################
	
	
	#get LVM root logical volume
	lv_query_result=($(sudo lvs | grep -E 'root' | awk '{printf "%s %s %s ", $1, $2, $3}'))
	lv=${lv_query_result[0]:-''}
	vg=${lv_query_result[1]:-''}
	#get corresponding physical volume
	pv_query_result=($(sudo pvs | grep -E "$vg" | awk '{printf "%s %s %s ", $1, $2, $3}'))
	pv=${pv_query_result[0]:-''}
	
	device=$pv
	part=$device
	disk_format_error=false
#	multiple_roots=false #not used yet
	
	
	if [[ $pv == *"crypt"* ]];then
		crypt=true
	else
		crypt=false
	fi
	
	DS "\nPartitionnement LVM détecté :"
	printf "\n > Volumes LVM :\n"
	printf "\n - volume logique sélectionné : "$lv
	printf "\n - groupe de volumes correspondant : "$vg
	printf "\n - volume physique correspondant : "$pv"\n\n"
	
	
	if [[ $device == *"mapper"* ]];then
		device=($(echo $device | cut -d '/' -f 4)) 
		device='/dev/'$device
		# /dev/mapper/quelquechose --> /dev/quelquechose
		#$device will be used as an argument for parted to resize the physical part, 
		#so we want to point towards the actual device and not the LUKS/LVM object referenced in /dev/mapper/
	fi
	
	if [[ $device == *"nvme"* ]];then
		#typically nvme parts are written as such : nvmeXnXpX (nvme0n1p3, nvme1n2p1...)
		#thus to get the device name we want to remove everything starting from the last 'p' : nvme0n1p3 --> nvme0n1
		#in case of an encrypted disk, the first line is also needed: nvme0n1p3_crypt -->nvme0n1p --> nvme0n1
		device=${device%[0-9]*} 
		device=${device%p*} 
		printf "NVMe détecté, disque : $device \n"
	elif [[ $device == *"sd"* ]];then
		#typically sd parts are written as such : sdAX (sdc1, sdb3...)
		#thus to get the device name we want to remove everything starting from the last number
		device=${device%[0-9]*} 
		printf "SD détecté, disque : $device \n"
	elif [[ $device == *"hd"* ]];then
		#typically sd parts are written as such : hdAX (hdc1, hdb3...)
		#thus to get the device name we want to remove everything starting from the last number
		device=${device%[0-9]*} 
		printf "HD détecté, disque : $device \n"
	else
		printf "\n////////////////////////////\nQuelque chose s'est mal passé : nom du disque incorrect ($device), sortie du programme d'extension\n////////////////////////////\n"
		device=''
		disk_format_error=true
	fi
	
	
	if [[ -n $device && $disk_format_error == false ]];then #stop if value is empty #+ multiple_roots check, if it is to be implemented
	
		part_number=$pv
		#for cases like 'nvme0n1p3_crypt', we begin by removing every non-digit character as long as there is a non-digit character at the end of the word
		while [[ $part_number =~ .*[^0-9]+$ ]];do # --> regexr.com
			part_number=${part_number%[^0-9]} 
		done
		part_number=${part_number##*[a-z]} #then we remove characters before the part number, i.e. we remove the longest sequence ending with a lowercase letter
		free_space_query_result=($(sudo parted $device print free -m | awk '{printf "%s ", $1}')) 
		free_space=""
		
		for i in "${!free_space_query_result[@]}"; do 
			
			#to choose if we display next part's size as 'free space' : 2 tests : 
			#is the part number right ? is the next part an actual part (ie. not #1) ?
			#for insight, check the output of : sudo parted $device print free -m
			line_starting_number=($(echo ${free_space_query_result[i]} | cut -d ':' -f 1))
			
			#setting a default value if the next line doesnt exist, otherwise the script will stop as it will eventually point to an undefined value
			following_line_starting_number=($(echo ${free_space_query_result[i+1]:-'99999::::::::'} | cut -d ':' -f 1)) 
			
			if [[ $line_starting_number = $part_number && $following_line_starting_number = 1 ]];then
			
				#double checking if the next part is actually 'free'
				double_check_query=($(echo ${free_space_query_result[i+1]} | cut -d ':' -f 5))
				
				if [[ $double_check_query == *"free"* ]];then
				
					free_space=($(echo ${free_space_query_result[i+1]} | cut -d ':' -f 4 )) 
					#4th column = part's size : in our case : free space
					DS "\nEspace non alloué trouvé après la partition $pv ($lv) : ${RESETCOLOR} $free_space\n"
					printf "Voulez vous étendre le volume $lv sur cet espace (o)? \nOu bien laisser les partitions ainsi (n)? " && read answerExt
					if [[ $answerExt == [Oo] ]];then
					
						#'growpart' can do all of this in a single line, but wanted to keep the script usable without additional installations
					
						hypothetical_next_part=${part##*/}
						hypothetical_next_part=${hypothetical_next_part%[0-9]*}
						hypothetical_next_part+=$((part_number+1))
						next_part_info_path='/sys/block/'${device##*/}'/'$hypothetical_next_part
						
						
						#in the case where there is a part after existing adjacent free space, 
						#we cannot simply resize to 100% : we must know the precise distance between parts
						if [[ -d $next_part_info_path ]];then
							printf "\nPartition existante au delà de l'espace libre : $hypothetical_next_part, extension jusqu'à celle-ci ! \n"
							next_part_start=$(($(cat $next_part_info_path/start)-40)) # -40 = safety offset
							
							sudo parted $device unit s resizepart $part_number $next_part_start && checkL
						else
							#extending the physical partition
							DSU "\nExtension de la partition $part sur l'ensemble du disque $device :"
							sudo parted -s $device resizepart $part_number 100% && checkL
						fi
					else
						printf "\n > Abandon de l'extension physique\n\n"
					fi
				fi
			fi
			done
			
			if [[ -z $free_space ]];then
				DS "\nPas d'espace physique libre trouvé après la partition $pv ($lv) : fin de l'extension physique\n" 	
			fi

			### LVM-LUKS specific manipulations below :
			# this code block is located outside of the
			# TODO : implement free space detection at the LVM level (compare outputs from 'lvs' and 'parted print free -m')
						
			if [[ $crypt ]];then
			#additional resizing of the ciphered volume (on which the LVM physical volume is implemented)
				DS "\nExtension du volume LUKS"
				sudo cryptsetup resize $pv && checkL
			else 
				printf "\nInstallation non-chiffrée !\n"
			fi
			
			#extending the LVM physical volume on the whole physical volume
			sudo pvresize $pv && checkL
			
			#extending the LVM logical volume on the whole volume group
			sudo lvextend -l +100%FREE /dev/mapper/$vg-$lv && checkL
			
			if [[ -z $free_space ]];then
				DS "\nPas d'espace libre trouvé après la partition $pv ($lv) : fin du programme d'extension\n" 	
			fi
		
	elif [[ $disk_format_error == true ]];then 
	#disk name is not in either 'nvme' 'sd' or 'hdd' formats
		printf "\n////////////////////////////\nQuelque chose s'est mal passé : variable 'device' nulle, sortie du programme d'extension\n////////////////////////////\n"
		
	fi
		
	## never happens (?) : (if this error detection is to be implemented, need to implement the test first)
	
#	elif [[ $multiple_roots == true ]];then
#	#error : more than the one and only root part
#		printf "\n////////////////////////////\nNombre de partitions montées sur '/' incorrect, sortie du programme d'extension\n////////////////////////////\n"
#	fi

}

extend_regular() {

#O.G. partinioning (non-LVM) : the procedure is simpler :)
	
	DS "\nPartitionnement classique détecté (non-LVM)\n"
	
	#looking for root's mount point, sometimes the first value isn't relevant, typically :
	
		#	NAME    	MOUNTPOINTS
		# 	nvme0n1p1     	/boot/efi
		#	nvme0n1p2  	/var/snap/firefox/common/host-hunspell
		#           		/
		#	sda1       	/media/commown/Ventoy1
		
	#here we insert random stuff to fill the blanks, otherwise the counters bellow would get desynchronized :
	#
	#	A	B	-->	A	B
	#		C	-->	C	=RIEN
	#
	disknames=($(lsblk -l | awk '{print funk(1)} function funk(n){return($n==""?"=RIEN ":$n)}'))
	mountpoints=($(lsblk -l | awk '{print funk(7)} function funk(n){return($n==""?"=RIEN ":$n)}'))
	root_found=false
#	multiple_roots=false
	disk_format_error=false
	
	
	if [[ ${#disknames[@]} -gt ${#mountpoints[@]} ]];then
		iterations_nb=${#disknames[@]}
	else 
		iterations_nb=${#mountpoints[@]} 
	fi
	
	for ((i=0; i<iterations_nb; i++));do		
		
		
		if [[ ${mountpoints[$i]} == '/' ]];then
			device=${disknames[$i]}
			
			root_found=true
		fi
		if [[ ${disknames[$i]} == '/' ]];then
			j=0
			while [[ $root_found == false && $disk_format_error == false ]];do
			
				((j++))				
				#setting a default value if the value doesnt exist, to avoid errors
				testing_value=($( echo ${disknames[$i-$j]:-'-_-'}))
				
				if [[ $testing_value == [a-z]* ]]; then
					device=$testing_value
					root_found=true
				fi
				if [[ $j -ge $iterations_nb || $testing_value == '-_-' ]];then
					printf "\n////////////////////////////\nQuelque chose s'est mal passé : la partition root n'a pas été trouvée !\nSortie du programme d'extension\n////////////////////////////\n"
					device=''
					disk_format_error=true
				fi		
			done
			
			#in this example (lsblk -l):
			
			#	NAME    	MOUNTPOINTS
			# 	nvme0n1p1     	/boot/efi
			#	nvme0n1p2  	/var/snap/firefox/common/host-hunspell
			#		   	/
				
			#root symbol '/' is the only value in its line, 
			#so the awk function sees it like it's in the 1st column of this line,
			#thus it gets stored in the 'disknames' array (1st column of the lsblk -l output) instead of 'mountpoints' (7th column)
			#thus the corresponding diskname is the previous entry in the 'disknames' array
		fi		
	done
		
	device='/dev/'$device
	part=$device
	
	if [[ $device == *"mapper"* ]];then #this case should not happen with regular (non-LVM) partitions
		disk_format_error=true
	fi
	
	if [[ $device == *"nvme"* ]];then
		#typically nvme parts are written as such : nvmeXnXpX (nvme0n1p3, nvme1n2p1...)
		#thus to get the device name we want to remove everything starting from the last 'p' : nvme0n1p3 --> nvme0n1
		#in case of an encrypted disk, the first line is also needed: nvme0n1p3_crypt -->nvme0n1p --> nvme0n1
		device=${device%[0-9]*} 
		device=${device%p*} 
		printf "NVMe détecté, disque : $device \n"
	elif [[ $device == *"sd"* ]];then
		#typically sd parts are written as such : sdAX (sdc1, sdb3...)
		#thus to get the device name we want to remove everything starting from the last number
		device=${device%[0-9]*} 
		printf "SD détecté, disque : $device \n"
	elif [[ $device == *"hd"* ]];then
		#typically sd parts are written as such : hdAX (hdc1, hdb3...)
		#thus to get the device name we want to remove everything starting from the last number
		device=${device%[0-9]*} 
		printf "HD détecté, disque : $device \n"
	else
		printf "\n////////////////////////////\nQuelque chose s'est mal passé : nom du disque incorrect ($device), sortie du programme d'extension\n////////////////////////////\n"
		device=''
		disk_format_error=true
	fi
	
	if [[ -n $device && $disk_format_error == false ]];then #stop if value is empty

		part_number=$part
		part_number=${part_number##*[a-z]} # we remove characters before the part number, i.e. we remove the longest sequence ending with a lowercase letter
		free_space_query_result=($(sudo parted $device print free -m | awk '{printf "%s ", $1}')) 
		free_space=""
		
			for i in "${!free_space_query_result[@]}"; do 
				
				#to choose if we display next part's size as 'free space' : 2 tests : 
				#is the part number right ? is the next part an actual part (ie. not #1) ?
				#for insight, check the output of : sudo parted $device print free -m
				line_starting_number=($(echo ${free_space_query_result[i]} | cut -d ':' -f 1))
				
				#setting a default value if the next line doesnt exist, otherwise the script will stop as it will eventually point to an undefined value
				following_line_starting_number=($(echo ${free_space_query_result[i+1]:-'99999::::::::'} | cut -d ':' -f 1)) 
				
				if [[ $line_starting_number = $part_number && $following_line_starting_number = 1 ]];then
				
					#double checking if the next part is actually 'free'S
					double_check_query=($(echo ${free_space_query_result[i+1]} | cut -d ':' -f 5))
					
					if [[ $double_check_query == *"free"* ]];then
					
						free_space=($(echo ${free_space_query_result[i+1]} | cut -d ':' -f 4 )) 
						#4th column = part's size : in our case : free space
						DS "\nEspace non alloué trouvé après la partition $part (root) : ${RESETCOLOR} $free_space\n"
						printf "Voulez vous étendre la partition $part sur cet espace (o)? \nOu bien laisser les partitions ainsi (n)? " && read answerExt
						if [[ $answerExt == [Oo] ]];then
							
							#'growpart' can do all of this in a single line, but wanted to keep the script usable without additional installations
						
							hypothetical_next_part=${part##*/}
							#current_part_info_path='/sys/block/'${device##*/}'/'$hypothetical_next_part
							hypothetical_next_part=${hypothetical_next_part%[0-9]*}
							hypothetical_next_part+=$((part_number+1))
							next_part_info_path='/sys/block/'${device##*/}'/'$hypothetical_next_part
							
							
							#in the case where there is a part after existing adjacent free space, 
							#we cannot simply resize to 100% : we must know the precise distance between parts
							if [[ -d $next_part_info_path ]];then
								printf "\nPartition existante au delà de l'espace libre : $hypothetical_next_part, extension jusqu'à celle-ci ! \n"
								printf " -- répondre 'oui' à la question suivante -- \n \n "
								#current_part_start=$(cat $current_part_info_path/start)
								#current_part_end=$((current_part_start + $(cat $current_part_info_path/size)))
								next_part_start=$(($(cat $next_part_info_path/start)-40)) #safety offset
								
								sudo parted $device unit s resizepart $part_number $next_part_start && checkL
							else
								#extending the physical partition
								printf "\nExtension de la partition $part sur l'ensemble du disque $device :"
								sudo parted -s $device resizepart $part_number 100% && checkL
							fi
							
							#extending the filesystem to fit the extra space
							sudo resize2fs $part && checkL
							
							
						else
							printf "\n > Abandon de l'extension\n\n"
						fi
					fi
				fi
				done
				
				if [[ -z $free_space ]];then
					DS "\nPas d'espace libre trouvé après la partition $part (root) : fin du programme d'extension\n" 	
				fi
			
	elif [[ $disk_format_error == true ]];then 
	#disk name is not in either 'nvme' 'sd' or 'hdd' formats
		printf "\n////////////////////////////\nQuelque chose s'est mal passé : variable 'device' nulle, sortie du programme d'extension\n////////////////////////////\n"	
	fi
		
	## never happens (?) : (if this error detection is to be implemented, need to implement the test first)
	
#	elif [[ $multiple_roots == true ]];then
#	#error : more than the one and only root part
#		printf "\n////////////////////////////\nNombre de partitions montées sur '/' incorrect, sortie du programme d'extension\n////////////////////////////\n"
#	fi
}
