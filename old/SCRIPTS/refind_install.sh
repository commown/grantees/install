#! /bin/bash

refind_install () {
    # rEFInd installation
    DS "rEFInd installation"
    pathR="/boot/efi/EFI/refind" && installR="DEBIAN_FRONTEND=noninteractive apt-get -y install refind"
    checkS "$installR" && checkDF "-d" "$pathR" 

    ##############################################################################################################################
    # ONLY IF you use UBUNTU folder with grubx64, mmx64, shimx64, etc. (Those three are using GRUB2 on autodetect or stanzas)
    #
    #
    # - Solution 1 : PREVENT GRUB2 to boot after refind installation (an UNRELIABLE solution):
    #     checkCL "apt-mark hold grub-efi-amd64 grub-efi-amd64-signed grub-common grub-efi-amd64-bin grub-common grub2-common"
    #
    # - Solution 2 : PREVENT chainloading by grub after rEFind (an UNRELIABLE solution)
    #     https://askubuntu.com/questions/869678/how-can-i-disable-grub-from-appearing-after-selecting-ubuntu-in-refind-menu?rq=1
    #     GRUB_HIDDEN_TIMEOUT_QUIET=true (/etc/default/grub) && update-grub
    #     # or/and : sed -i '/^GRUB_TIMEOUT=/c\GRUB_TIMEOUT=0' /etc/default/grub && update-grub
    ##############################################################################################################################

    # Theme Refind
    DS "Theme rEFInd" && link="https://gitlab.com/commown/refind-theme-regular"
    rInclude="include themes/refind-theme-regular/theme.conf" && pathRT="$pathR/themes" && name="refind-theme-regular"
    # Important: Delete older installed versions of this theme before you proceed any further.
    # rm -rf $path/{regular-theme,refind-theme-regular}
    checkCL "rm -rf $pathRT" "mkdir -p $pathRT"
    # Append file "append.conf" to "refind.conf"
    checkCL "git clone --quiet $link $pathRT/$name" && checkDF "-f" "$pathR/refind.conf"
    checkS "cat $dir/RESOURCES/append.conf >> $pathR/refind.conf"
    # Remove unused directories and files.
    checkCL "rm -rf $pathRT/$name/{src,.git}" "rm $pathRT/$name/install.sh" && checkDF "-d" "$pathRT/$name"
    regex="TERMINAL (root shell)"
    vOS=$(cat /etc/os-release | grep -o -P '(?<=PRETTY_NAME=").*(?=")') # display version OS linux
    #Removes line 3 (minimal) & replace ro->rw & "..." by "TERMINAL (root shell) & linux version"  [s/ : substitution, /gm : global multi ligne]
    checkS "sed -ie '3d' /boot/refind_linux.conf"
    sed -ie "2 s/\(\"\).*\(\" \)/\1${regex}\2/;2 s/ro\ /rw\ /" /boot/refind_linux.conf >/dev/null && checkL # checkS not valid with special regex
    sed -ie "1 s/\(\"\).*\(\" \)/\1${vOS}\2/" /boot/refind_linux.conf >/dev/null && checkL   # checkS not valid with special regex
    checkS "rm /boot/refind_linux.confe" # sed is buggy and creates a temp file, so we delete the file after sed
    # rename os_linux.png => os_linux_penguin.png
    pathI="$pathRT/refind-theme-regular/icons"
    checkCL "mv $pathI/128-48/os_linux.png $pathI/128-48/os_linux_penguin.png"
    # Icones & background
    DS "Background modification & selection for rEFInd"
    checkCL "cp $dir/IMAGES/os_linux.png $pathI/128-48/os_linux.png"            # terminal icon
    checkCL "cp $dir/IMAGES/selection.png $pathI/selection.png"                 # selection icon
    checkCL "cp $dir/IMAGES/backgroundCommown.png $pathI/backgroundCommown.png" # background refind
    checkDF "-f" "$pathI/128-48/os_linux.png" && checkDF "-f" "$pathI/selection.png" && checkDF "-f" "$pathI/backgroundCommown.png"
    # Modify background (1024×768px) :
    path_regex="themes\/refind-theme-regular\/icons"
    regexBg="s/banner $path_regex\/128-48\/bg.png/banner $path_regex\/backgroundCommown.png\n#banner $path_regex\/128-48\/bg.png/"
    regexS_b="s/selection_big $path_regex\/128-48\/selection-big.png/selection_big $path_regex\/selection.png/"
    regexS_s="s/selection_small $path_regex\/128-48\/selection-small.png/selection_small $path_regex\/selection.png/"
    checkS "sed -i -e \"$regexBg;$regexS_b;$regexS_s\" $pathRT/refind-theme-regular/theme.conf"
}
